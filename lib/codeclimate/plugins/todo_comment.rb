# File: lib/codeclimate/plugins/todo_comments.rb

module CodeClimate
    module Plugins
      class TodoComments < Plugins::Engine
        def run(files)
          issues = []
  
          files.each do |file|
            next unless file.content
  
            file.content.each_with_index do |line_content, line_number|
              if line_content.include?("TODO")
                issues << build_issue(file.path, line_number + 1)
              end
            end
          end
  
          issues
        end
  
        private
  
        def build_issue(file_path, line_number)
          description = "TODO comment found in #{file_path} at line #{line_number}"
          Issue.new(description, nil, file_path, line_number)
        end
      end
    end
  end
  