module CodeClimate
  class SpecificFunctionNameChecker < Source::Engine
    def run
      function_name = config['function_name'] || 'showAddBookForm' # Default to 'specific_function' if no function name is provided
      processed_files.each do |src/main/java/servlet, *.java|
        lines = contents.split("\n")
        lines.each_with_index do |line, index|
          if line.include?(function_name)
            add_issue("Found #{function_name} in #{path} at line #{index + 1}")
          end
        end
      end
    end
  end
end
